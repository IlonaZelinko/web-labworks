<?php

/**
 * Created by PhpStorm.
 * User: Ilona
 * Date: 3/25/2017
 * Time: 6:58 PM
 */
class ImageHandler
{
    protected $uploadedFile;
    protected $name;
    protected $type;
    protected $tmpName;
    protected $width;
    protected $heigh;



    public function __construct($fileName, $fileType,$fileTempName, $width, $heigh) {
        $this->name = $fileName;
        $this->type = $fileType;
        $this->tmpName = $fileTempName;
        $this->width = $width;
        $this->heigh = $heigh;
    }
    protected function getImageFromFile() {

            if (file_exists($this->tmpName)) {
                $this->name = 'img/'.$this->name;
                switch($this->type){
                    case 'image/jpeg':
                        $this->uploadedFile = imagecreatefromjpeg($this->name);
                        break;
                    case 'image/gif':
                        $this->uploadedFile = imagecreatefromgif($this->name);
                        break;
                    case 'image/png':
                        $this->uploadedFile = imagecreatefrompng($this->name);
                        break;
                    default:
                        return false;
                        break;
                }
            return true;
            }
            else{
                return false;
            }
    }

    public function resizeImage() {
        if($this->getImageFromFile() != false && $this->width!= null && $this->heigh != null){

            $coeff =  $this->getCoefficient();

            $newWidth = imagesx($this->uploadedFile) * $coeff;
            $newHeigh = imagesy($this->uploadedFile) * $coeff;
            $dest = imagecreatetruecolor($newWidth, $newHeigh);
            $waterMark = imagecreatefrompng('img/watermark.png');
            $resizedWatermark = imagecreatetruecolor(imagesx($waterMark)*$coeff, imagesy($waterMark)*$coeff);


            imagecopyresized($dest, $this->uploadedFile, 0, 0, 0, 0,
                $newWidth, $newHeigh, imagesx($this->uploadedFile), imagesy($this->uploadedFile));

            imagecopyresized($dest, $waterMark,$newWidth-imagesx($resizedWatermark),$newHeigh-imagesy($resizedWatermark),0,0, imagesx($waterMark)*$coeff, imagesy($waterMark)*$coeff,imagesx($waterMark),imagesy($waterMark));

            $this->saveImage($dest, 'resized/'.$this->name,$this->type);
            imagedestroy($dest);
            imagedestroy($this->uploadedFile);
            return 'resized/'.$this->name;
        }
    }

    public function getCoefficient() {

            $min = min($this->width, $this->heigh);

            $minUploaded = max(imagesx($this->uploadedFile),imagesy($this->uploadedFile));

            return $min/$minUploaded;
    }

    public function saveImage($dest, $name,$type) {
        switch($type){
            case 'image/jpeg':
                imagejpeg($dest,$name);
                break;
            case 'image/gif':
                imagegif($dest,$name);
                break;
            case 'image/png':
                imagepng($dest,$name);
                break;
            default:
                return false;
                break;
        }
    }
}