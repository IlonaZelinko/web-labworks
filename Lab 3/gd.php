<?php
/**
 * Created by PhpStorm.
 * User: Ilona
 * Date: 3/17/2017
 * Time: 12:26 PM
 */
include "ImageHandler.php";
//echo phpinfo();
if (isset($_FILES['imageToUpload']['name'])){

    $name = $_FILES["imageToUpload"]["name"];
    $tmp_name = $_FILES['imageToUpload']['tmp_name'];
    $type = $_FILES['imageToUpload']['type'];
    $width = $_POST['finalWidth'];
    $heigh = $_POST['finalHeigh'];

    $imageHandler = new ImageHandler($name,$type,$tmp_name,$width,$heigh);
    $resizedImgPath = $imageHandler->resizeImage();
    session_start();
    $_SESSION['resizedImg'] = $resizedImgPath;
    header("Location: index.php");
    exit();

}

?>