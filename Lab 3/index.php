<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Chart</title>
    <link rel="stylesheet" href="css/style.css">
    <script src="js/jquery-3.1.1.js"></script>
    <script src="js/scripts.js"></script>
</head>
<body>
<div id="demo">
    <canvas id="canvas"></canvas>
    <div id="imgResize">
        <form enctype="multipart/form-data" method="post" action="gd.php">
            <div>
                <label for="image">Choose image here:</label>
                <input id="image" type="file" name="imageToUpload">
            </div>
            <label>Input size of finnal image:</label>
            <div>
                <label for="width">Width:</label>
                <input id="width" type="number" name="finalWidth" min="1">
            </div>
            <div>
                <label for="heigh">Heigh:</label>
                <input id="heigh" type="number" name="finalHeigh" min="1">
            </div>
            <input type="submit" value="Upload">
        </form>
        <?php
        session_start();
        if(isset($_SESSION['resizedImg']))
        {
            echo '<img src="'.$_SESSION['resizedImg'].'"/>';
        }
        ?>

    </div>

</div>
</body>
</html>