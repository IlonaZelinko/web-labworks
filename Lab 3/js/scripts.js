/**
 * Created by Ilona on 3/13/2017.
 */
$(document).ready(function(){
    $.ajax({
        method: 'get',
        url: 'ajax.php',
        datatype: 'text',
        success: function(result) {
            DrawChart(JSON.parse(result));
        }
    })
});

function DrawChart(data) {
    var padding = 30;
    var canvas = document.getElementById('canvas');
    var context = canvas. getContext('2d');
    canvas.width= canvas.clientWidth;
    canvas.height = canvas.clientHeight;
    context.strokeRect(0,0,canvas.width,canvas.height);

    DrawCoordinates(context,data, canvas.width, canvas.height, padding);
    var distanceY = GetDistanceY(canvas.height, padding, GetHorisontalLinesNumber(data));
    var distanceX = GetDistanceX(canvas.width, padding, data);

    for(var i = distanceX+padding, j = 0; j < data.length; i+=distanceX,j++ ) {
        DrawLine(context,i-distanceX/2,canvas.height-padding-data[j],i - distanceX/2+ distanceX,canvas.height-padding-data[j+1] ,'Green' );
        DrawCircle(context,i-distanceX/2,canvas.height-padding-data[j] , 3);
    }
}

function GetHorisontalLinesNumber(data) {
    var maxValue = Math.max.apply(null, data);
    return Math.floor(maxValue/50)+1;
}

function DrawCoordinates(context,data, width, height, padding ){

    var hlCount = GetHorisontalLinesNumber(data);
    var distanceY = GetDistanceY(height, padding, hlCount);
    var distanceX = GetDistanceX(width,padding, data);

    DrawLine(context, padding,padding, padding,height-padding);
    //DrawLine(context, padding,height-padding, width-padding,height-padding);

    for(var i = 0, j = 0; i <= hlCount; i++, j+=distanceY){
        DrawLine(context, padding, height-padding-j, width-padding, height-padding-j, 'Black');
        WriteText(context,j,5, height-padding-j);
    }
    for(var i = 1, j = distanceX+padding; i <= data.length; i++, j+=distanceX){
        WriteText(context,i,j-distanceX/2,height-10);
    }
}
function GetDistanceX(width, padding, data){
    return Math.floor((width-2*padding)/data.length);
}
function GetDistanceY(height, padding, hlCount) {
    return Math.floor((height-2*padding)/hlCount);
}
function DrawLine(ctx, startX, startY, endX, endY, color){
    ctx.strokeStyle = color;
    ctx.beginPath();
    ctx.moveTo(startX,startY);
    ctx.lineTo(endX,endY);
    ctx.stroke();
}

function DrawCircle(ctx, x, y, radius)
{
    ctx.fillStyle = 'Green';
    ctx.beginPath();
    ctx.arc(x, y, radius, 0, Math.PI * 2, false);
    ctx.fill();
}
function WriteText(ctx, text, x, y){
    ctx.fillStyle = "black";
    ctx.font = "14px Arial";
    ctx.fillText(text,x,y);
}

