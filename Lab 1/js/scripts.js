$(document).ready(function() {

    $('.btn-more').on("click", function(e) {
        e.preventDefault();
        $.ajax({
            method: 'get',
            url: "ajax.php",
            data: {
                id: $(this).attr('id')
            },
            dataType: "text",
            success: function(result){
                $('#demo-content').html(HtmlToJson(result));
                $('#demo').css("display", "block");
            }

        });

    });
});


/*function loadData(clicked_id) {
    var ajx = new XMLHttpRequest();
    ajx.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {

            document.getElementById("demo").innerHTML += HtmlToJson(this.responseText);
            document.getElementById("demo").style.display = "block";
        }
    };
    ajx.open("GET", "ajax.php?id=" + clicked_id, true);
    ajx.send();
}*/

function HtmlToJson($jsonString) {
    var obj = JSON.parse($jsonString);
    var html = "";
    html += "<div>" + obj.name + "</div>";
    html += "<img src='" + obj.img +"'>";
    html += "<div>" + obj.price + "</div>";
    html += "<div>" + obj.sale + "</div>";
    html += "<div>" + obj.safeguard + "</div>";
    html += "<div>" + obj.yearOutput + "</div>";
    html += "<div>" + obj.os + "</div>";

    return html;
}

function closeWindow(){
    document.getElementById("demo").style.display = "none";
}


function getInfo(xml, id){

    var xmlDoc = xml.responseXML;

    var x = xmlDoc.getElementsByTagName("phone");

    for (i = 0; i <x.length; i++)
    {
        if( x[i].getElementsByTagName("serialNumber")[0].childNodes[0].nodeValue == id)
        {
            var phone = {
        img: x[i].getAttribute("img"),
        name: x[i].getElementsByTagName("name")[0].childNodes[0].nodeValue,
        serialNumber: x[i].getElementsByTagName("serialNumber")[0].childNodes[0].nodeValue,
        price: x[i].getElementsByTagName("price")[0].childNodes[0].nodeValue,
        sale: x[i].getElementsByTagName("price")[0].getAttribute("sale"),
        safeguard: x[i].getElementsByTagName("safeguard")[0].childNodes[0].nodeValue,
        yearOutput: x[i].getElementsByTagName("yearOutput")[0].childNodes[0].nodeValue,
        os: x[i].getElementsByTagName("os")[0].childNodes[0].nodeValue,
    };
        var outJSON = JSON.stringify(phone);
        return outJSON;

    }
    }
}