<!DOCTYPE html>
<?php
include 'process.php';
?>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Dental HELTH</title>
        <link rel="stylesheet" type="text/css" href="css/style.css">
        <script src="js/jquery-3.1.1.js"></script>
        <script type="text/javascript" src="js/scripts.js"></script>

    </head>
    <body>
        <header class="clear-fix">
            <div class="tooth-img"><img src="img/tooth.png" alt="Tooth" width="173px" height="142px"></div>
            <div class="header-main">
                <div>
                    <img class="description" src="img/Header.png" alt="Dental HELTH">  
                    <p class="description text">These innovations have served to enable us to<br> better live our mission; "To build partnerships<br> that 
                        improve our customers' earnings by co-managing<br> their supply, equipment and practice management needs" . </p> 
                </div>
                <div class="description date">Wednesday, October 23, 2002</div>
            </div>
            <div class="header-buttons">
                <div class="user-menu">
                    <a href="empty.html"><img src="img/helpBtn.png" alt="Help"></a>
                    <a href="#"><img src="img/AboutUsBtn.png" alt="About us"></a>
                    <a href="#"><img src="img/ContactBtn.png" alt="Contact"></a>
                </div>
                <div class="search-menu">
                    <div class="search-menu word">SEARCH:</div>
                    <input class="search-menu input" type="text" name="search">
                <div class="search-menu button"><a href="#"><img src="img/OKbtn.png" alt="Ok"></a></div>
                </div>
            </div>
        </header>
        <div class="nav">
            <nav>
                <div class="nav-bar extend"><img src="img/threeDots.png" class="point"><a href="#">&nbsp;Merchandise</a></div>
                <div class="nav-bar"><img src="img/threeDots.png" class="point"><a href="#">&nbsp;Equipment</a></div>
                <div class="nav-bar"><img src="img/threeDots.png" class="point"><a href="#">&nbsp;Service</a></div>
                <div class="nav-bar"><img src="img/threeDots.png" class="point"><a href="#">&nbsp;Seminars</a></div>
                <div class="nav-bar"><img src="img/threeDots.png" class="point"><a href="#">&nbsp;Practice Development</a></div>
                <div class="nav-bar"><img src="img/threeDots.png" class="point"><a href="#">&nbsp;Painless Web</a></div>
                <div class="nav-bar"><img src="img/threeDots.png" class="point"><a href="#">&nbsp;BluChips Rewards</a></div>
                <div class="nav-bar"><img src="img/threeDots.png" class="point"><a href="#">&nbsp;Earnings Builders</a></div>
                <div class="nav-bar"><img src="img/threeDots.png" class="point"><a href="#">&nbsp;Interlink</a></div>
                <div class="nav-bar"><img src="img/threeDots.png" class="point"><a href="#">&nbsp;Featured Products</a></div>
                <div class="nav-bar"><img src="img/threeDots.png" class="point"><a href="#">&nbsp;Partners</a></div>
                <div class="nav-bar"><img src="img/threeDots.png" class="point"><a href="#">&nbsp;International</a></div>
                <div class="nav-bar"><img src="img/threeDots.png" class="point"><a href="#">&nbsp;Government</a></div>
                <div class="nav-bar"><img src="img/threeDots.png" class="point"><a href="#">&nbsp;Handpiece repairs</a></div>   
            </nav>
            <div class="features">
                <div class="img-features"><img src="img/SpotlingFeatures.png" alt="Spotliting features"></div>
                <img src="img/box.png" alt="Example">
                <div class="litle-header">Seminars in your area </div>
                <div class="text-features">Dates, topics, locations,<br> &amp; prices. Save 10% when<br> 3 or more sign-up!</div>
                
                <div class="litle-header second">Topics, Trends,<br> Techniques </div>
                <div class="text-features">Don't let your patients<br> give you the brush-off.<br> Increase patient <br>acceptance!</div>
            </div>
        </div>
        <main>
             <h1>Список товаров</h1>
            <?php
            $cl = new Processor();
            $arr = $cl->GetData();
            foreach ($arr as $item)
            {
                echo  '<div class="container">
                <img src="'.$item->img.'" alt="'.$item->name.'" class="phone-img">
                <div class="name">'.$item->name.'</div>
                <div class="pr">Price:</div>
                <div class="price-info">
                <span class="price">'.$item->price.'</span>';

                if($item->sale != "")
                {
                  echo '<div class="arrow-left"></div>
                <span class="sale">'.$item->sale.'</span>';
                }
                echo '</div>
            <input type="button" value="More..."class="btn-more" id="'.$item->serialNumber.'">
            </div>'
                ;
            }?>
        <div id="demo">
            <div id="close" onclick="closeWindow()"></div>
            <div id="demo-content"></div>
        </div>
        </main>
    </body>
</html>
