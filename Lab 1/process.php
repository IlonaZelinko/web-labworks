<?php
include 'PhoneInfo.php';

class Processor {
    public $phonesInfo;

    public function GetData()
    {
        $this->phonesInfo = [];

        $xmlDocument = new DOMDocument();
        $xmlDocument -> load("phones.xml");
        $x = $xmlDocument->documentElement;
        //echo var_dump($xmlDoc);
        $phones = $x->getElementsByTagName("phone");

        foreach($phones as $phone) {
            $phoneInfo = new PhoneInfo();
            $childs = $phone->childNodes;
            $phoneInfo->img = $phone->getAttribute('img');
            foreach ($childs as $node) {
                switch ($node->nodeName) {
                    case 'name':
                        $phoneInfo->name = $node->nodeValue;
                        break;
                    case 'serialNumber':
                        $phoneInfo->serialNumber = $node->nodeValue;
                        break;
                    case 'price':
                        $phoneInfo->price = $node->nodeValue;
                        $phoneInfo->sale = $node->getAttribute('sale');
                        break;
                    case 'safeguard':
                        $phoneInfo->safeguard = $node->nodeValue;
                        break;
                    case 'yearOutput':
                        $phoneInfo->yearOutput = $node->nodeValue;
                        break;
                    case 'os':
                        $phoneInfo->os = $node->nodeValue;
                        break;
                }
            }
            $this->phonesInfo[] = $phoneInfo;
        }

        return  $this->phonesInfo;
    }
    function GetProductById($serialNumber)
    {
        foreach ($this->phonesInfo as $phone)
        {
            if($phone->serialNumber == $serialNumber)
            {
                return json_encode($phone);
            }
        }
    }

}
?>