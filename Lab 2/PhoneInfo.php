<?php

/**
 * Created by PhpStorm.
 * User: Ilona
 * Date: 2/11/2017
 * Time: 11:18 AM
 */
class PhoneInfo
{
    public $name;
    public $img;
    public $serialNumber;
    public $price;
    public $sale;
    public $safeguard;
    public $yearOutput;
    public $os;
}