$(document).ready(function() {
    $('#first-inp').on("change",function(){
        validate1('#first-inp','#first');
    });
    $('#second-inp').on("change",function(){
        validate1('#second-inp','#second');
    });
    $(".calculator").on('click', '#submit-btn', function (){
        calculate();
    });
});


function validate1(id, index) {
    var a =$(id).val();
    if($.trim(a)!== '')
    {
        if( !$.isNumeric(a))
        {
            $(id).addClass("incorect-input");
            $(index).show();

        }
        else{
            if($(id).hasClass("incorect-input"))
            {
                $(id).removeClass("incorect-input");
                $(index).hide();
            }
        }
    }
}

function check(){
    var a = $('#est-result-inp').val();
    var b = $('#result-inp').val();
    if(a===b){
        $('#estresult').text("Ответы совпадают");
        $('#estresult').css('color',"#0d6409");
        $('#estresult').show();
    }
    else{
        $('#estresult').text("Ответы не совпадают");
        $('#estresult').css('color',"#b70415");
        $("#estresult").show();
    }
}
function calculate() {

    var userChoise = $("select option:selected").text().toLowerCase();
    $('#result-inp').val('');
    if($.isNumeric($('#first-inp').val())== true && $.isNumeric($('#second-inp').val()))
    {
        var a = parseFloat($('#first-inp').val());
        var b = parseFloat($('#second-inp').val());
        switch(userChoise)
        {
            case "sum":
                $('#result-inp').val(a+b);
                break;
            case "sub":
                $('#result-inp').val(a-b);
                break;
            case "div":
                if(b!= 0)
                {
                    $('#result-inp').val(a/b);
                }
                else{
                    $('#result-inp').val("Деление на 0");
                }
                break;
            case "mul":
                $('#result-inp').val(a*b);
                break;
            case "pow":
                $('#result-inp').val(Math.pow(a,b));
                break;
            case "sqrt":
                if(a>=0)
                {
                    $('#result-inp').val(Math.sqrt(a));
                }
                else{
                    $('#result-inp').val("Число меньше 0");
                }
                break;
        }
        check();
    }

}

function isNumber(n){
    var result = n.match('/[^0-9]/');
    if(result != null)
    {
        return true;
    }
    else
    {
        return false;
    }
}

