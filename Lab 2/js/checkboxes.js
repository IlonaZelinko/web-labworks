/**
 * Created by Ilona on 3/10/2017.
 */
$(document).ready(function(){
    $('#choose-all').on('click', function () {
        chooseAll();
    });
    $('#cancel').on('click', function () {
       cancelAll();
    });
    $('#invert').on('click', function () {
        invert();
    });
});

function chooseAll(){
    $('.left-part input:checkbox').prop('checked', true);
}
function cancelAll(){
    $('.left-part input:checkbox').prop('checked', false);
}
function invert() {
    $('.left-part input:checkbox').each(function () {
        if($(this).is(':checked')) {
            $(this).prop('checked', false);
        }
        else{
            $(this).prop('checked', true);
        }
    })
}