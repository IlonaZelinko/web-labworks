<!DOCTYPE html>
<?php
include 'process.php';
?>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Dental HELTH</title>
        <link rel="stylesheet" type="text/css" href="css/style.css">
        <link rel="stylesheet" type="text/css" href="css/jquery.fancybox.min.css">
        <link rel="stylesheet" type="text/css" href="css/jquery.bxslider.css">
        <script src="js/jquery-3.1.1.js"></script>
        <script src="js/jquery.bxslider.min.js"></script>
        <script src="js/scripts.js"></script>
        <script src="js/checkboxes.js"></script>
        <script src="js/photo.js"></script>
        <script src="js/jquery.fancybox.min.js"></script>

    </head>
    <body>
        <header class="clear-fix">
            <div class="tooth-img"><img src="img/tooth.png" alt="Tooth" width="173px" height="142px"></div>
            <div class="header-main">
                <div>
                    <img class="description" src="img/Header.png" alt="Dental HELTH">  
                    <p class="description text">These innovations have served to enable us to<br> better live our mission; "To build partnerships<br> that 
                        improve our customers' earnings by co-managing<br> their supply, equipment and practice management needs" . </p> 
                </div>
                <div class="description date">Wednesday, October 23, 2002</div>
            </div>
            <div class="header-buttons">
                <div class="user-menu">
                    <a href="empty.html"><img src="img/helpBtn.png" alt="Help"></a>
                    <a href="#"><img src="img/AboutUsBtn.png" alt="About us"></a>
                    <a href="#"><img src="img/ContactBtn.png" alt="Contact"></a>
                </div>
                <div class="search-menu">
                    <div class="search-menu word">SEARCH:</div>
                    <input class="search-menu input" type="text" name="search">
                <div class="search-menu button"><a href="#"><img src="img/OKbtn.png" alt="Ok"></a></div>
                </div>
            </div>
        </header>
        <div class="nav">
            <nav>
                <div class="nav-bar extend"><img src="img/threeDots.png" class="point"><a href="#">&nbsp;Merchandise</a></div>
                <div class="nav-bar"><img src="img/threeDots.png" class="point"><a href="#">&nbsp;Equipment</a></div>
                <div class="nav-bar"><img src="img/threeDots.png" class="point"><a href="#">&nbsp;Service</a></div>
                <div class="nav-bar"><img src="img/threeDots.png" class="point"><a href="#">&nbsp;Seminars</a></div>
                <div class="nav-bar"><img src="img/threeDots.png" class="point"><a href="#">&nbsp;Practice Development</a></div>
                <div class="nav-bar"><img src="img/threeDots.png" class="point"><a href="#">&nbsp;Painless Web</a></div>
                <div class="nav-bar"><img src="img/threeDots.png" class="point"><a href="#">&nbsp;BluChips Rewards</a></div>
                <div class="nav-bar"><img src="img/threeDots.png" class="point"><a href="#">&nbsp;Earnings Builders</a></div>
                <div class="nav-bar"><img src="img/threeDots.png" class="point"><a href="#">&nbsp;Interlink</a></div>
                <div class="nav-bar"><img src="img/threeDots.png" class="point"><a href="#">&nbsp;Featured Products</a></div>
                <div class="nav-bar"><img src="img/threeDots.png" class="point"><a href="#">&nbsp;Partners</a></div>
                <div class="nav-bar"><img src="img/threeDots.png" class="point"><a href="#">&nbsp;International</a></div>
                <div class="nav-bar"><img src="img/threeDots.png" class="point"><a href="#">&nbsp;Government</a></div>
                <div class="nav-bar"><img src="img/threeDots.png" class="point"><a href="#">&nbsp;Handpiece repairs</a></div>   
            </nav>
            <div class="features">
                <div class="img-features"><img src="img/SpotlingFeatures.png" alt="Spotliting features"></div>
                <img src="img/box.png" alt="Example">
                <div class="litle-header">Seminars in your area </div>
                <div class="text-features">Dates, topics, locations,<br> &amp; prices. Save 10% when<br> 3 or more sign-up!</div>
                
                <div class="litle-header second">Topics, Trends,<br> Techniques </div>
                <div class="text-features">Don't let your patients<br> give you the brush-off.<br> Increase patient <br>acceptance!</div>
            </div>
        </div>
        <main>
            <form action="#" method="GET" class="calculator">
                <div class="left-part">
                    <input type="checkbox" name="ch1" id="ch1" value="bike1"><label for="ch1">I have a bike</label><br>
                    <input type="checkbox" name="ch2" value="bike2">I have a bike<br>
                    <input type="checkbox" name="ch3" value="bike3">I have a bike<br>
                    <input type="checkbox" name="ch4" value="bike4">I have a bike<br>
                    <input type="checkbox" name="ch5" value="bike5">I have a bike<br>
                    <input type="checkbox" name="ch6" value="bike6">I have a bike<br>
                    <input type="checkbox" name="ch7" value="bike7">I have a bike<br>
                    <input type="checkbox" name="ch8" value="bike8">I have a bike<br>
                    <input type="checkbox" name="ch9" value="bike9">I have a bike<br>
                    <input type="checkbox" name="ch10" value="bike10">I have a bike<br>
                    <input type="button" id="choose-all" value="Choose all">
                    <input type="button" id="invert" value="Invert">
                    <input type="button" id="cancel" value="Cancel all">
                </div>
                <div class="right-part">
                    <div><label for="first-inp">First value:</label></div>
                    <input id="first-inp" type="text" name="first-value" placeholder="45">
                    <div id="first" class="error">Ошибка! Нужно ввести число!</div>
                    <div>Second Value:</div>
                    <input id="second-inp" type="text" name="second-value" placeholder="45"><br>
                    <div id="second" class="error">Ошибка! Нужно ввести число!</div>
                    <select id="operation">
                        <option value="sum">Sum</option>
                        <option value="sub">Sub</option>
                        <option value="div">Div</option>
                        <option value="mul">Mul</option>
                        <option value="pow">Pow</option>
                        <option value="sqrt">Sqrt</option>
                    </select>
                    <div>Estimated result:</div><input id="est-result-inp" type="text" name="estimated-result" placeholder="Your result">
                    <div id="estresult" class="error">Ответы не совпадают!</div>
                    <div>Result:</div>
                    <input id="result-inp" type="text" name="result" readonly>
                    <div><input id="submit-btn" type="button" value="Submit"></div>
                </div>
            </form>
            <div class="clear-fix"></div>
            <div class="photo-galery">
                <a class="fancy-img" href="img/IMG_2136.JPG" data-fancybox="group" data-caption="Caption #1"><img class="single_image" src="img/IMG_2136.JPG" alt=""/></a>
                <a class="fancy-img" href="img/IMG_2142.JPG" data-fancybox="group" data-caption="Caption #2"><img class="single_image" src="img/IMG_2142.JPG" alt=""/></a>
                <a class="fancy-img" href="img/IMG_2156.JPG" data-fancybox="group" data-caption="Caption #3"><img class="single_image" src="img/IMG_2156.JPG" alt=""/></a>
                <a class="fancy-img" href="img/IMG_2171.JPG" data-fancybox="group" data-caption="Caption #4"><img class="single_image" src="img/IMG_2171.JPG" alt=""/></a>
                <a class="fancy-img" href="img/IMG_2212.JPG" data-fancybox="group" data-caption="Caption #5"><img class="single_image" src="img/IMG_2212.JPG" alt=""/></a>

            </div>
            <ul class="bxslider">
                <li><img src="img/IMG_2136.JPG" /></li>
                <li><img src="img/IMG_2142.JPG" /></li>
                <li><img src="img/IMG_2156.JPG" /></li>
                <li><img src="img/IMG_2171.JPG" /></li>
            </ul>
        </main>

    </body>
</html>
