<!DOCTYPE html>
<?php
include "Classes/Client.php";
$client = new Client();
$categories = $client->GetCategiries();
if(isset($_GET['category'])){
    $products = $client->GetProductsInCategory($_GET["category"]);
}else
{
    $products = $client->GetProductsInCategory($categories[0]["CategoryName"]);
}
?>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Dental HELTH</title>
        <link rel="stylesheet" type="text/css" href="css/style.css">
        <script src="js/jquery-3.1.1.js"></script>
        <script type="text/javascript" src="js/scripts.js"></script>
    </head>
    <body>
       <div class="categiries">
           <?php
           foreach ($categories as $category)
           {
               //Спросить как указать адрес текущей страницы
               echo '<a class="category" href="http://localhost:8080/'.'?category='.$category["CategoryName"].'">'.$category["CategoryName"].'</a>';
           }
           echo '<hr>
        </div>';
        echo '<div class="products">';
           for ($i = 0; $i< count($products); $i++)
           {
               echo '<div class="item">
                     <div class="product-name">'.$products[$i]["ProductName"].'</div>
                     <img class="pr-img"src="'.$products[$i]["ImagePath"].'" alt="'.$products[$i]["ProductName"].'">
                     <div class="price">'.$products[$i]["Price"].' грн.</div>
                    <div class="btn-more"> <input type="button" id="'.$products[$i]["ID"].'" value="more..." class="btn"></div>
                </div>';
           }
           ?>
        </div>
    </body>
</html>
