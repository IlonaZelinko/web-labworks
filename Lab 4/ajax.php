<!DOCTYPE html>
<?php
include "Classes/Client.php";
?>
<html>
<head>
    <meta charset="UTF-8">
    <title>Phones</title>
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <script src="js/jquery-3.1.1.js"></script>
    <script type="text/javascript" src="js/scripts.js"></script>
</head>
<body>

<?php

if(isset($_GET['id']))
{
    $q = $_GET['id'];
    $cl = new Client();

    $data = $cl->GetProductInfo($q);
    $img = $cl->GetProductsImages($q);
echo '<div class="container"><p class="product-name-sp">' . $data[0]['ProductName'] . '</p>
    <p class="price">' . $data[0]['Price'] . '</p>
    <p class="description-sp"> ' . $data[0]['Description'] . '</p>
    <p class="year-output-sp">' . $data[0]['YearOutput'] . '</p>
    <p class="os-sp">' . $data[0]['OS'] . '</p>
    <div class="images">';

foreach ($img as $image) {
    echo '<img class="single_image" src="'.$image['ImagePath'].'" alt=""/></a>';

}
echo '</div></div>
<div class="clear-fix"></div>
<form action="comment.php?id='.$q.'" method="POST" id="form-comment">
    <label for="us-name" >Your name</label>
    <input type="text" id="us-name" name="name">
    <label for="us-comment">Your comment</label>
    <textarea rows="4" cols="50" id="us-comment" name="comment"></textarea>
    <input type="submit" value="Submit" name="add-comment">
</form>';
}
?>
<?php
$responses = $cl->GetResponsesByProductId($q);
if($responses != null)
{
   echo' <p class="product-name-sp">Отзывы клиентов</p>';
    for($i = count($responses)-1; $i >= 0; $i--)
    {
        echo '<div class="response">
       
        <div class="name">'.$responses[$i]["AuthorName"].'</div>
        <div class="comment">'.$responses[$i]["Response"].'</div>
        </div>';
    }
}
?>
</body>
</html>

