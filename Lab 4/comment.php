<?php
/**
 * Created by PhpStorm.
 * User: Ilona
 * Date: 4/1/2017
 * Time: 11:41 AM
 */

include "Classes/Response.php";

if (!empty($_POST['add-comment'])) {
    if($_POST["name"]!="" && $_POST["comment"]!="" && $_GET["id"]!=""){
        $comment = new Response($_POST["name"],$_POST["comment"], $_GET["id"]);

        $comment->AddComment();

    }
    header('Location: ajax.php?id='.$_GET["id"].'');
}
?>