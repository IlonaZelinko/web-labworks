<?php

/**
 * Created by PhpStorm.
 * User: Ilona
 * Date: 3/29/2017
 * Time: 3:23 PM
 */
include "Classes/DataDisplay.php";
//include "Classes/DataEntry.php";
class Client
{
    private $sql;
    public function GetCategiries()
    {
        $this->sql = "SELECT DISTINCT CategoryName FROM products";
        $data = new DataDisplay($this->sql);
        return $data->result;

    }
    public function GetProductsInCategory($category)
    {
        $this->sql = "SELECT products.ID,ProductName,Price,ImagePath FROM `products` INNER JOIN images ON CategoryName ='".$category."' AND products.ID=images.ProductID GROUP BY products.ID";
        $data = new DataDisplay($this->sql);
        return $data->result;
    }

    public function GetProductInfo($id)
    {
        $this->sql = "SELECT * FROM `products` WHERE ID= '".$id."'";
        $result = new DataDisplay($this->sql);
        return $result->result;
    }
    public function GetProductsImages($id)
    {
        $this->sql = "SELECT ImagePath FROM images WHERE ProductId='".$id."'";
        $result = new DataDisplay($this->sql);
        return $result->result;
    }

    public function GetResponsesByProductId($id)
    {
        $this->sql = "SELECT AuthorName, Response FROM responses WHERE ProductID='".$id."'";
        $result = new DataDisplay($this->sql);
        return $result->result;

    }

}