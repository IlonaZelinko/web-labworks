<?php

/**
 * Created by PhpStorm.
 * User: Ilona
 * Date: 3/29/2017
 * Time: 4:39 PM
 */
include 'Classes/DbConnector.php';

class DataDisplay
{
    private $bd;
    public $result;
    public function __construct($query)
    {
        $this->bd = DbConnector::connect();
        $this->DispalyData($query);
        //$this->bd->close();
    }

    public function DispalyData($query)
    {
        mysqli_set_charset($this->bd,"utf8");
        $rows = array();
        $result = mysqli_query($this->bd,$query);

        if($result === false) {
            $this->result = false;
        }
        while ($row = mysqli_fetch_assoc($result)) {
            $rows[] = $row;
        }
        $this->result = $rows;
    }
}