<?php

/**
 * Created by PhpStorm.
 * User: Ilona
 * Date: 3/29/2017
 * Time: 3:40 PM
 */
class DbConnector
{
    protected static $connection;

    public static function connect() {

        if(!isset(self::$connection)) {

            $config = parse_ini_file('config.ini');
            self::$connection = new mysqli('localhost',$config['username'],$config['password'],$config['dbname']);
            mysqli_set_charset(self::$connection,"utf8");
        }

        if(self::$connection === false) {

            return false;
        }
        return self::$connection;
    }



}