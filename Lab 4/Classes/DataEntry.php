<?php

/**
 * Created by PhpStorm.
 * User: Ilona
 * Date: 3/29/2017
 * Time: 4:56 PM
 */
include 'Classes/DbConnector.php';

class DataEntry
{
    private $db;
    public $result;
    public function __construct($query)
    {
        $this->db = DbConnector::connect();
        $this->InsertData($query);
        $this->db->close();
    }

    public function InsertData($query)
    {
        $this->result = mysqli_query($this->db,$query);
    }
}