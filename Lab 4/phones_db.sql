-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Хост: 127.0.0.1
-- Время создания: Апр 01 2017 г., 21:19
-- Версия сервера: 10.1.16-MariaDB
-- Версия PHP: 5.6.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `phones_db`
--

-- --------------------------------------------------------

--
-- Структура таблицы `images`
--

CREATE TABLE `images` (
  `ID` int(11) NOT NULL,
  `ImagePath` text NOT NULL,
  `ProductID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `images`
--

INSERT INTO `images` (`ID`, `ImagePath`, `ProductID`) VALUES
(0, 'https://media.carphonewarehouse.com/is/image/cpw2/Samsung_GalaxyS6_DESIGN%20DISPLAY_01?$JPG6Column$', 0),
(1, 'http://i-cdn.phonearena.com/images/articles/241101-image/Galaxy-C5.jpg', 0),
(2, 'http://cdn2.gsmarena.com/vv/pics/samsung/samsung-galaxy-note-edge-02.jpg', 0),
(3, 'http://drop.ndtv.com/TECH/product_database/images/1023201560845PM_635_samsung_galaxy_on7.jpeg', 0),
(4, 'http://cdn2.gsmarena.com/vv/pics/huawei/huawei-p8-0.jpg', 1),
(5, 'http://drop.ndtv.com/TECH/product_database/images/4162015113251AM_635_huawei_ascend_p8.jpeg', 1),
(6, 'http://my-live-03.slatic.net/p/2/huawei-ascend-p8-lite-dual-sim-lte-16gb-gold-3823-93708321-3b35c396ad820494128352dc1babfac3-catalog_233.jpg', 1),
(7, 'https://www.dpreview.com/files/p/E~products/zte_axon7/shots/fffdd834fd3b46039c4c4d1b3816fd22.jpeg', 2),
(8, 'https://3.bp.blogspot.com/-rbQfyUi_CxI/V-afZVDQ9gI/AAAAAAAAmGg/yXQH26256fQUREotldJqVzzRQhfkQX1FgCPcB/s640/ZTE%2BAXON7.jpg', 2),
(9, 'https://smhn.info/wp-content/uploads/2016/10/AXON7-654x462.jpg', 2),
(10, 'http://drop.ndtv.com/TECH/product_database/images/172015121609PM_635_lenovo_tab2_a7_30.jpeg', 3),
(11, 'http://gadgetstouse.com/wp-content/uploads/2015/01/lenovo-a600.jpg', 3),
(12, 'http://img01.ibnlive.in/ibnlive/uploads/2015/01/2-Lenovo-A600-310115.jpg', 3),
(13, 'http://cdn2.gsmarena.com/vv/bigpic/apple-iphone-7r4.jpg', 4),
(14, 'https://support.apple.com/content/dam/edam/applecare/images/en_US/iphone/featured-content-iphone-transfer-content-ios10_2x.png', 4),
(15, 'https://blogs-images.forbes.com/ewanspence/files/2016/06/uSwitch_iPhone8_render1-1200x577.jpg', 4),
(16, 'http://www.wlivenews.com/wp-content/uploads/2013/11/Lenovo-Laptops1.jpg', 5),
(17, 'http://img01.ibnlive.in/ibnlive/uploads/875x584/jpg/2015/06/Lenovo-yoga-laptop-100615.jpg', 5),
(18, 'https://www.wired.com/2015/04/macbook-alternatives/', 5),
(19, 'http://www.laptopmag.com/images/uploads/ppress/44886/samsung-ativ-book-9-plus-bigcl.jpg', 6),
(20, 'http://www.laptopmag.com/images/uploads/4358/g/samsung-ativ-book-9-plus-g02.jpg', 6),
(21, 'http://compareindia.news18.com/media/gallery/images/2012/may/np300v5a_1_101244419077.jpg', 6),
(22, 'http://www.notebookcheck.net/fileadmin/_processed_/csm_Vorneschr_2cb0caef47.jpg', 7),
(23, 'https://cdn.macrumors.com/article-new/2013/09/macbook_air_2015_elcap_roundup_header-800x459.jpg', 7),
(24, 'https://store.storeimages.cdn-apple.com/4974/as-images.apple.com/is/image/AppleInc/aos/published/images/m/ac/macbook/air/macbook-air-gallery2-2014?wid=1292&hei=766&fmt=jpeg&qlt=95&op_sharpen=0&resMode=bicub&op_usm=0.5,0.5,0,0&iccEmbed=0&layer=comp&.v=1476297407703', 7),
(25, 'http://i-cdn.phonearena.com/images/phones/50899-xlarge/Lenovo-TAB-2-A7-10-1.jpg', 8),
(26, 'http://drop.ndtv.com/TECH/product_database/images/172015121609PM_635_lenovo_tab2_a7_30.jpeg', 8),
(27, 'http://images.fonearena.com/blog/wp-content/uploads/2015/02/Lenovo-Tab-2-A7-301.jpg', 8),
(28, 'http://93b00a24bd188d49d428-5353a8a2802c90717395ac989c1a694a.r74.cf1.rackcdn.com/wp-content/uploads/sites/3/image788.jpg', 8),
(29, 'http://s7d2.scene7.com/is/image/SamsungUS/SM-T580NZKAXAR-05-71016?$product-details-jpg$', 9),
(30, 'http://images.samsung.com/is/image/samsung/uk-galaxy-tab-a-10-1-2016-t585-sm-t585nzkabtu-000000006-dynamic-black?$PD_GALLERY_JPG$', 9),
(31, 'https://cdn.theunlockr.com/wp-content/uploads/2013/04/Samsung-Galaxy-Tab-10.14.jpg', 9),
(32, 'http://i2.rozetka.ua/goods/1837756/cube_t12_u12gt_images_1837756496.jpg', 10),
(33, 'http://gloimg.gearbest.com/gb/pdm-product-pic/Maiyang/2016/12/30/goods-img/1487055584232762123.jpg', 10),
(34, 'http://www.cube-tablet.com/media/catalog/product/cache/1/image/1800x/040ec09b1e35df139433887a97daa66f/c/u/cube-t12-tablet-2.jpg', 10);

-- --------------------------------------------------------

--
-- Структура таблицы `products`
--

CREATE TABLE `products` (
  `ID` int(11) NOT NULL,
  `CategoryName` varchar(255) NOT NULL,
  `ProductName` varchar(255) NOT NULL,
  `Price` int(11) NOT NULL,
  `Description` text NOT NULL,
  `YearOutput` year(4) NOT NULL,
  `OS` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `products`
--

INSERT INTO `products` (`ID`, `CategoryName`, `ProductName`, `Price`, `Description`, `YearOutput`, `OS`) VALUES
(0, 'Смартфоны', 'Samsung galaxy', 5299, 'Экран (5.5", Super AMOLED Plus, 1280x720)/ Samsung Exynos 7580 (1.5 ГГц)/ основная камера: 13 Мп, фронтальная камера: 5 Мп/ RAM 1.5 ГБ/ 16 ГБ встроенной памяти + microSD/SDHC (до 128 ГБ)/ 3G/ GPS/ поддержка 2х SIM-карт (Micro-SIM)/ Android 5.1 (Lollipop) / 3000 мА*ч\r\nПодробнее: http://rozetka.com.ua/mobile-phones/c80003/preset=smartfon/', 2015, 'Android'),
(1, 'Смартфоны', 'Huawei', 1234, 'Экран (5.5", Super AMOLED Plus, 1280x720)/ Samsung Exynos 7580 (1.5 ГГц)/ основная камера: 13 Мп, фронтальная камера: 5 Мп/ RAM 1.5 ГБ/ 16 ГБ встроенной памяти + microSD/SDHC (до 128 ГБ)/ 3G/ GPS/ поддержка 2х SIM-карт (Micro-SIM)/ Android 5.1 (Lollipop) / 3000 мА*ч\r\nПодробнее: http://rozetka.com.ua/mobile-phones/c80003/preset=smartfon/', 2015, 'Android'),
(2, 'Смартфоны', 'Axon7', 3456, 'Экран (5.5", Super AMOLED Plus, 1280x720)/ Samsung Exynos 7580 (1.5 ГГц)/ основная камера: 13 Мп, фронтальная камера: 5 Мп/ RAM 1.5 ГБ/ 16 ГБ встроенной памяти + microSD/SDHC (до 128 ГБ)/ 3G/ GPS/ поддержка 2х SIM-карт (Micro-SIM)/ Android 5.1 (Lollipop) / 3000 мА*ч\r\nПодробнее: http://rozetka.com.ua/mobile-phones/c80003/preset=smartfon/', 2016, 'Android'),
(3, 'Смартфоны', 'Lenovo', 7934, 'Экран (5.5", Super AMOLED Plus, 1280x720)/ Samsung Exynos 7580 (1.5 ГГц)/ основная камера: 13 Мп, фронтальная камера: 5 Мп/ RAM 1.5 ГБ/ 16 ГБ встроенной памяти + microSD/SDHC (до 128 ГБ)/ 3G/ GPS/ поддержка 2х SIM-карт (Micro-SIM)/ Android 5.1 (Lollipop) / 3000 мА*ч\r\nПодробнее: http://rozetka.com.ua/mobile-phones/c80003/preset=smartfon/', 2016, 'Android'),
(4, 'Смартфоны', 'Iphone', 19543, 'Экран (5.5", Super AMOLED Plus, 1280x720)/ Samsung Exynos 7580 (1.5 ГГц)/ основная камера: 13 Мп, фронтальная камера: 5 Мп/ RAM 1.5 ГБ/ 16 ГБ встроенной памяти + microSD/SDHC (до 128 ГБ)/ 3G/ GPS/ поддержка 2х SIM-карт (Micro-SIM)/ Android 5.1 (Lollipop) / 3000 мА*ч\r\nПодробнее: http://rozetka.com.ua/mobile-phones/c80003/preset=smartfon/', 2017, 'iOS'),
(5, 'Ноутбуки', 'Lenovo', 8900, 'Экран 15.6” (1920x1080) Full HD, глянцевый с антибликовым покрытием / Intel Core i5-6200U (2.3 - 2.8 ГГц) / RAM 8 ГБ / SSD 128 ГБ / AMD Radeon R5 M430, 2 ГБ / без ОД / LAN / Wi-Fi / Bluetooth / веб-камера / DOS / 1.96 кг / черный\r\nПодробнее: http://rozetka.com.ua/notebooks/c80004/filter/preset=653/', 2015, 'Windows 10'),
(6, 'Ноутбуки', 'Samsung', 20773, 'Экран 15.6” (1920x1080) Full HD, глянцевый с антибликовым покрытием / Intel Core i5-6200U (2.3 - 2.8 ГГц) / RAM 8 ГБ / SSD 128 ГБ / AMD Radeon R5 M430, 2 ГБ / без ОД / LAN / Wi-Fi / Bluetooth / веб-камера / DOS / 1.96 кг / черный\r\nПодробнее: http://rozetka.com.ua/notebooks/c80004/filter/preset=653/', 2016, 'Windows 10'),
(7, 'Ноутбуки', 'Apple MacBook Air 13', 26999, 'Экран 13.3" (1440x900) WXGA+ LED, глянцевый / Intel Core i5 (1.6 - 2.7 ГГц) / RAM 8 ГБ / SSD 128 ГБ / Intel HD Graphics 6000 / без ОД / Wi-Fi / Bluetooth / веб-камера / OS X El Capitan / 1.35 кг\r\nПодробнее: http://rozetka.com.ua/notebooks/c80004/filter/preset=653/', 2017, 'Mac OS'),
(8, 'Планшеты', 'Lenovo Tab 2 ', 6400, 'Экран 10.1" (1920x1200) IPS емкостный, MultiTouch / MediaTek MT8732 (1.7 ГГц) / RAM 2 ГБ / 32 ГБ встроенной памяти + microSD / Wi-Fi b/g/n / Bluetooth 4.0 / 3G / LTE / основная камера 8 Мп, фронтальная - 5 Мп / GPS / ГЛОНАСС / Beidou / Android 4.4 (KitKat) / 509 г / темно-синий\r\nПодробнее: http://rozetka.com.ua/tablets/c130309/filter/35714=45474/', 2015, 'Android'),
(9, 'Планшеты', 'Samsung Galaxy Tab A 10.1', 8999, 'Экран 10.1" (1920х1200) емкостный MultiTouch / Exynos 7870 (1.6 ГГц) / RAM 2 ГБ / 16 ГБ встроенной памяти + microSD / 3G / LTE / Wi-Fi 802.11 a/b/g/n/ac / Bluetooth 4.1 / основная камера 8 Мп, фронтальная 2 Мп / GPS / ГЛОНАСС / Android 6.0 (Marshmallow) / 525 г / синий\r\nПодробнее: http://rozetka.com.ua/tablets/c130309/filter/35714=45474/', 2016, 'Android'),
(10, 'Планшеты', 'Cube T12', 2964, 'Экран 10.1" IPS (1280x800) емкостный, MultiTouch / MediaTek MT8321 (1.3 ГГц) / RAM 1 ГБ / 16 ГБ встроенной памяти + microSD / 3G / Wi-Fi / Bluetooth 4.0 / основная камера 5 Мп, фронтальная 0.3 Мп / GPS / Android 6.0 (Marshmallow) / 555 г / серебристый\r\nПодробнее: http://rozetka.com.ua/tablets/c130309/filter/35714=45474/', 2016, 'Android');

-- --------------------------------------------------------

--
-- Структура таблицы `responses`
--

CREATE TABLE `responses` (
  `ID` int(11) NOT NULL,
  `AuthorName` varchar(255) NOT NULL,
  `Response` text NOT NULL,
  `ProductID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `responses`
--

INSERT INTO `responses` (`ID`, `AuthorName`, `Response`, `ProductID`) VALUES
(1, 'Ilona', 'text', 1),
(2, 'vdsb', 'sdbbs', 0),
(3, 'fbdbd', 'dfgdgdr', 1),
(4, 'thtrr', 'htrhrhrj', 0),
(5, '.$this->authorName.', '.$this->responseText.', 1),
(6, 'Ilona', 'efwegweg', 2),
(7, 'Ilona', 'efwegweg', 2),
(8, 'fsdgdg', 'dfgdgd', 0),
(9, 'Нил', 'ывауоац', 2),
(10, '123', 'qwqrwqfrwre', 2),
(11, 'Ivan', 'sdfsgrge', 9),
(12, '123', 'fgrgrtgtrhgtr', 9),
(13, '', '', 9),
(14, '', '', 9);

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `images`
--
ALTER TABLE `images`
  ADD PRIMARY KEY (`ID`),
  ADD UNIQUE KEY `ID` (`ID`);

--
-- Индексы таблицы `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`ID`),
  ADD UNIQUE KEY `ID` (`ID`);

--
-- Индексы таблицы `responses`
--
ALTER TABLE `responses`
  ADD PRIMARY KEY (`ID`),
  ADD UNIQUE KEY `ID` (`ID`),
  ADD UNIQUE KEY `ID_2` (`ID`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `responses`
--
ALTER TABLE `responses`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
